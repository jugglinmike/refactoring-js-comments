# Refactoring JavaScript to support (and reduce!) code comments

When you're working with JavaScript, you may come across code that does a lot
of stuff on a small number of lines. Check out this line from a Resilient
Coders demo app:

```js
res.writeHead(200, {'Content-Type': 'text/html'});
```

Your first inclination might be to add comments that explain what that code
does. This is a good idea! The trouble is, for complicated lines of code like
that, it can be a bit tough to describe each part separately. Look at how the
following comment block has to explicitly reference different parts of the
line:

```js
// `writeHead` sends the HTTP status code and headers to the client.
// `200` is the HTTP status code.
// The Object is a collection of headers (just one header in this case).
// `Content-Type` is a header that describes the "media type" of the response.
// `text/html` is the type used to describe HTML files.
res.writeHead(200, {'Content-Type': 'text/html'});
```

**Always remember that you can rearrange the code to make it easier to
understand.** Changing the code in this way is sometimes called "refactoring,"
and by giving the code more room to "breathe," it lets you be more precise with
your comment placement.

One kind of refactoring that's pretty universal is introducing variables for
values. `200`, and `{'Content-Type': 'text/html'}` are two values in this
sample code; we'll start with `200`:

```js
// `200` is the HTTP status code.
const x = 200;

// `writeHead` sends the HTTP status code and headers to the client.
// The Object is a collection of HTTP headers (just one header in this case).
// `Content-Type` is a header that describes the "media type" of the response.
// `text/html` is the type used to describe HTML files.
res.writeHead(x, {'Content-Type': 'text/html'});
```

This refactoring is already an improvement because it let us document the value
`200` directly, and it shrank the paragraph of comments before `writeHead`. But
why choose a generic variable name like `x` when you could use any name at all?
Why not choose something that describes the value?

```js
// `200` is the HTTP status code.
const status = 200;

// `writeHead` sends the HTTP status code and headers to the client.
// The Object is a collection of HTTP headers (just one header in this case).
// `Content-Type` is a header that describes the "media type" of the response.
// `text/html` is the type used to describe HTML files.
res.writeHead(status, {'Content-Type': 'text/html'});
```

Remember that variable names can be as long as you'd like them to be. Even
though some developers prefer to keep them short, **you should make variable
names as long as you need to make yourself comfortable reading the code**. When
using a more expressive name, you might not even need comments explaining the
variable's purpose! This is called "self-documenting code."

```js
const httpStatusCodeSuccess = 200;

// `writeHead` sends the HTTP status code and headers to the client.
// The Object is a collection of HTTP headers (just one header in this case).
// `Content-Type` is a header that describes the "media type" of the response.
// `text/html` is the type used to describe HTML files.
res.writeHead(httpStatusCodeSuccess, {'Content-Type': 'text/html'});
```

If we apply this same approach to `{'Content-Type': 'text/html'}`, we end up
with code that's functionally identical to the original, but that's a whole lot
easier to understand:

```js
const httpStatusCodeSuccess = 200;
const httpHeaders = {
  // `Content-Type` is a header that describes the "media type" of the response.
  // `text/html` is the type used to describe HTML files.
  'Content-Type': 'text/html'
};
// `writeHead` sends the HTTP status code and headers to the client.
res.writeHead(httpStatusCodeSuccess, httpHeaders);
```

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
